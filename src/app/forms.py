from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length
from app.models import User

class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submitbtn = SubmitField("SignIn")

class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    password2 = PasswordField(
            "Repeat Password", validators=[DataRequired(), EqualTo("password")]
    )
    submitbtn = SubmitField("Register")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Username already exists")

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Email already exists")

class PostForm(FlaskForm):
    post = TextAreaField("Say something", validators=[
        DataRequired(), Length(min=1, max=140)]
    )
    submitbtn = SubmitField("Submit")

class NameForm(FlaskForm):
    display_name = StringField("Display name", validators=[DataRequired()])
    submitbtn = SubmitField("Change Name")
    def validate_username(self, displayname):
        user = User.query.filter_by(displayname=displayname.data).first()
        if user is not None:
            raise ValidationError("Display name is already in use")
