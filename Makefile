# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
APP=isec3004.assignment

all: build

build:
	docker build --rm --tag=$(APP) .
	docker image prune -f

test: run-detached func vuln stop

func:
	python3 test/test_functionality.py

vuln:
	python3 test/test_vulnerabilities.py

run:
	docker run -p 0.0.0.0:8080:8080/tcp -it --name $(APP) --rm $(APP)

run-detached:
	docker run -p 0.0.0.0:8080:8080/tcp -itd --name $(APP) --rm $(APP)

stop:
	docker stop $(APP)

clean:
	docker image rm $(APP)
	docker system prune

.PHONY: all test clean

sean:
	ssh -p2220 bruce@localhost './ccsep.sh'