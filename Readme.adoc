= ISEC3004 Assignment - 0xTBD

== Description

This is a message board web application in which a user can register for an account, login, make posts on the message board and change their display name which is shown when making posts. The master branch is vulnerable to Cross-Site Request Forgery (CSRF) attacks.

== Running the Program

. Build the container with ```make```
. Run the web application with ```make run```
. Run the tests with ```make test```

The web app is accessible on ```localhost:8080```

== Detection

You can detect CSRF vulnerabilities by checking the requests used for changing the state of values which include the GET and POST requests found below. If these requests dont have unique identifiers and dont require you to log in then it can indicate that it is vulnerable.

You can also check source code for a unique identifier or requirement to re-enter your password.

== Exploiting

=== GET Request

The GET request vulnerability can be triggered using the following url:
    ```http://localhost:8080/name?display_name=changednamevuln&submit=Change+Name```

=== POST Request

The POST request vulnerability can be triggered by loading the html file ```post-test.html``` found in ```/test``` in a seperate tab in the browser you are interacting with the web application on.

=== Email Payloads

The payloads from the presentation can be found under ```test/email```. In order to send these, open the ```.html``` files in a browser, ctrl + A, ctrl + C and then ctrl + V in the body of the message you'd like to send over an email client of your choice. 

== Patching

. The first patch can be found in app.py where the line ```app.config.from_mapping({'WTF_CSRF_ENABLED':False})``` can be removed.
. The second patch can be found in the config.py file where the following lines can be removed.
``
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_HTTPONLY = False
REMEMBER_COOKIE_SECURE = False
REMEMBER_COOKIE_HTTPONLY = False
JWT_COOKIE_CSRF_PROTECT = False
WTF_CSRF_ENABLED = False
``
. The final patch can be found in the config.py file where you can add the following lines.
``
SESSION_COOKIE_SECURE=True
SESSION_COOKIE_HTTPONLY=True
SESSION_COOKIE_SAMESITE='Strict'
``

== License

See link:LICENSE[]
