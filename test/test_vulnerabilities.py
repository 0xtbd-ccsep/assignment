import requests
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
import time
import os

driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

requests.post("http://localhost:8080/register", data={'username':'test', 'email':'test@test.com', 'password':'test', 'password2':'test', 'submit':'Register'})

driver.get("http://localhost:8080/login")

usernameElement = driver.find_element_by_name("username")
passwordElement = driver.find_element_by_name("password")

time.sleep(1)
usernameElement.send_keys("test")
time.sleep(1)
passwordElement.send_keys("test")
time.sleep(1)

driver.find_element_by_id("submitbtn").submit()
time.sleep(1)

driver.execute_script("window.open()")
driver.switch_to.window(driver.window_handles[1])
driver.get('http://localhost:8080/name?display_name=changednamevuln&submit=Change+Name')
time.sleep(2)
driver.close()
driver.switch_to.window(driver.window_handles[0])
time.sleep(1)

driver.get("http://localhost:8080/index")

if "changednamevuln" in driver.page_source:
    print("\nFAILED : GET Vulnerability Test")
else:
    print("\nPASS : GET Vulnerability Test")
time.sleep(2)

driver.execute_script("window.open()")
driver.switch_to.window(driver.window_handles[1])
html_file = os.getcwd() + "/" + "test/post-test.html"
driver.get("file://" + html_file)
time.sleep(2)
driver.close()
driver.switch_to.window(driver.window_handles[0])

driver.get("http://localhost:8080/index")

if "haxored" in driver.page_source:
    print("FAILED : POST Vulnerability Test\n")
else:
    print("PASS : POST Vulnerability Test\n")
time.sleep(2)

driver.close()