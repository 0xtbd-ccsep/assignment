from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
import time

driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

driver.get("http://localhost:8080/register")

usernameElement = driver.find_element_by_name("username")
emailElement = driver.find_element_by_name("email")
password1Element = driver.find_element_by_name("password")
password2Element = driver.find_element_by_name("password2")

time.sleep(1)
usernameElement.send_keys("test")
time.sleep(1)
emailElement.send_keys("test@test.com")
time.sleep(1)
password1Element.send_keys("test")
time.sleep(1)
password2Element.send_keys("test")
time.sleep(1)

driver.find_element_by_id("submitbtn").submit()
time.sleep(1)
try:
    driver.find_element_by_name("remember_me")
    print("\nPASS : Registering Test")
except:
    print("\nFAILED : Registering Test\n")
    driver.quit()
    exit()

driver.get("http://localhost:8080/login")

usernameElement = driver.find_element_by_name("username")
passwordElement = driver.find_element_by_name("password")

time.sleep(1)
usernameElement.send_keys("test")
time.sleep(1)
passwordElement.send_keys("test")
time.sleep(1)

driver.find_element_by_id("submitbtn").submit()
time.sleep(1)

try:
    driver.find_element_by_id("post").submit()
    print("PASS : Login Test")
except:
    print("FAILED: Login Test\n")
    driver.quit()
    exit()

driver.get("http://localhost:8080/index")

postElement = driver.find_element_by_name("post")

time.sleep(1)
postElement.send_keys("test post")
time.sleep(1)

driver.find_element_by_id("submitbtn").submit()
time.sleep(2)

if "test post" in driver.page_source:
    print("PASS : Post Test")
else:
    print("FAILED : Post Test\n")
    driver.quit()
    exit()

driver.get("http://localhost:8080/name")

nameElement = driver.find_element_by_name("display_name")

time.sleep(1)
nameElement.send_keys("changedname")
time.sleep(1)

driver.find_element_by_id("submitbtn").submit()
time.sleep(1)

if "to changedname" in driver.page_source:
    print("PASS : Change Name Test\n")
else:
    print("FAILED: Change Name Test\n")
    driver.quit()
    exit()

driver.close()